package  com.utbm.to.admin.core.catalogue.core.entity;


/***********************************************************************
 * Module:  Reservation.java
 * Author:  
 * Purpose: Defines the Class Reservation
 ***********************************************************************/

import java.util.*;

public class Reservation {
   private long codeReservation;
   private Date dateDeReservation;
   private Client client;
   private Article article;
   
    public Reservation() {
    }

    public Reservation(long codeReservation, Date dateDeReservation, Client client) {
        this.codeReservation = codeReservation;
        this.dateDeReservation = dateDeReservation;
        this.client = client;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

   

    public long getCodeReservation() {
        return codeReservation;
    }

    public void setCodeReservation(long codeReservation) {
        this.codeReservation = codeReservation;
    }

    public Date getDateDeReservation() {
        return dateDeReservation;
    }
    public void setDateDeReservation(Date dateDeReservation) {
        this.dateDeReservation = dateDeReservation;
    }
    
    public Article getArticle() {
		return article;
	}
    public void setArticle(Article article) {
		this.article = article;
	}
    
 
}