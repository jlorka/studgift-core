package com.utbm.to.admin.core.catalogue.core.dao;


import com.utbm.to.admin.core.catalogue.core.entity.LigneReservation;
import com.utbm.to.admin.core.catalogue.core.entity.Reservation;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 
 */
public class LigneReservationDao {
    
    // Enregistrer une ligne reservation
    public static void registerLigneReservation(LigneReservation ligneReservation) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(ligneReservation);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Lire une Ligne reservation
    public static LigneReservation readerLigneReservation(int idLigneReservation) {
        LigneReservation ligneReservation = new LigneReservation();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            ligneReservation = (LigneReservation) session.get(LigneReservation.class, idLigneReservation);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return ligneReservation;
    }

    // Recupérer la liste des ligneReservation
     public static  List<LigneReservation> getAllLigneReservation() {
        List<LigneReservation> ligneReservationList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery("from LigneReservation");
         ligneReservationList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return ligneReservationList;
    }
     
     
     
     
     
     public static  List<LigneReservation> getAllLigneReservationByReservation(Reservation reservation) {
        List<LigneReservation> reservationList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from LigneReservation where RESERV_CODE =?");
         query.setParameter(0,reservation.getCodeReservation());
         reservationList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservationList;
    }
     
     
     
     
     
     
     public static void main(String[] args) {
             //registerLigneReservation(new  LigneReservation(Long.parseLong("1"), Float.NaN, new Date(), new Date()));
         for(LigneReservation u:LigneReservationDao.getAllLigneReservation()){
            System.out.println(" LigneReservation :" + u.getQuantiteReservee());
        }
    }
    
}
