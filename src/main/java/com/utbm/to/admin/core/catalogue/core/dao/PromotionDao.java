package com.utbm.to.admin.core.catalogue.core.dao;


import com.utbm.to.admin.core.catalogue.core.entity.Promotion;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 
 */
public class PromotionDao {
    
    //Enregistrer une Promotion
    public static void registerPromotion(Promotion promotion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(promotion);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Lire une promotion
    public static Promotion readerPromotion(long idPromotion) {
        Promotion promotion = new Promotion();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            promotion = (Promotion) session.get(Promotion.class, idPromotion);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return promotion;
    }

    // Recupérer la liste des Promotions
     public static  List<Promotion> getAllPromotion() {
        List<Promotion> promotionList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Promotion");
         promotionList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return promotionList;
    }
     
     
     public static void main(String[] args) {
             //registerPromotion(new  Promotion(Long.parseLong("1"), Float.NaN, new Date(), new Date()));
         for(Promotion u:PromotionDao.getAllPromotion()){
            System.out.println(" Promotion :" + u.getCodePromotion());
        }
    }
    
}
