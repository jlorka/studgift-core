package  com.utbm.to.admin.core.catalogue.core.entity;

/***********************************************************************
 * Module:  LigneReservation.java
 * Author:  Marina
 * Purpose: Defines the Class LigneReservation
 ***********************************************************************/

import java.util.*;

public class LignePromotion {
   private Date debutPromotion;
   private Date finPromotion;
   private Promotion promotion;
   private Article article;
   private Float valeurPromotion;

    public LignePromotion() {
    }

    public LignePromotion(Date debutPromotion, Date finPromotion, Promotion promotion, Article article, Float valeurPromotion) {
        this.debutPromotion = debutPromotion;
        this.finPromotion = finPromotion;
        this.promotion = promotion;
        this.article = article;
        this.valeurPromotion = valeurPromotion;
    }

    public Float getValeurPromotion() {
        return valeurPromotion;
    }

    public void setValeurPromotion(Float valeurPromotion) {
        this.valeurPromotion = valeurPromotion;
    }

    
    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Date getDebutPromotion() {
        return debutPromotion;
    }

    public Date getFinPromotion() {
        return finPromotion;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setDebutPromotion(Date debutPromotion) {
        this.debutPromotion = debutPromotion;
    }

    public void setFinPromotion(Date finPromotion) {
        this.finPromotion = finPromotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

}