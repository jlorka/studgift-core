package com.utbm.to.admin.core.catalogue.core.dao;


import com.utbm.to.admin.core.catalogue.core.entity.Article;
import com.utbm.to.admin.core.catalogue.core.entity.Catalogue;
import com.utbm.to.admin.core.catalogue.core.entity.Categorie;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 
 */
public class ArticleDao {
    
    // Enregister un Article
    public static void registerArticle(Article article) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            article.setCatalogue(null);
            session.beginTransaction();
            session.persist(article);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Modifier un article
    public static void updateArticle(Article article) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            readerArticle(article.getReferenceArticle());
            article.setCatalogue(null);
            session.beginTransaction();
            session.merge(article);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Lire Un Article
    public static Article readerArticle(Long idArticle) {
        Article article = new Article();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            article = (Article) session.get(Article.class, idArticle);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return article;
    }

    // Afficher la liste des Articles
     public static  List<Article> getAllArticle() {
        List<Article> articleList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery("from Article");
         articleList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return articleList;
    }
     
     
     public static void main(String[] args) {
         System.out.println("Tester Article  :"+readerArticle(Long.parseLong("1")).getActivate());
    	 //registerArticle(new Article(null, "designation",Float.parseFloat("100"), (long)2, "deztails", "Marque", null,"adresse", "90000", "Belfort", null, null));
     }
    
}
