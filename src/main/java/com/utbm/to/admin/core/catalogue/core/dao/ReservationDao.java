package com.utbm.to.admin.core.catalogue.core.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.utbm.to.admin.core.catalogue.core.entity.Article;
import com.utbm.to.admin.core.catalogue.core.entity.Client;
import com.utbm.to.admin.core.catalogue.core.entity.Reservation;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 
 */
public class ReservationDao {
    
    // Lire une reservation
    public static void registerReservation(Reservation reservation) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(reservation);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Lire une reservation
    public static Reservation readerReservation(Long idReservation) {
        Reservation reservation = new Reservation();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            reservation = (Reservation) session.get(Reservation.class, idReservation);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservation;
    }

    // Recupérer la liste des Reservations
     public static  List<Reservation> getAllReservation() {
        List<Reservation> reservationList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Reservation");
         reservationList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservationList;
    }
    
     // Recupérer la liste des Reservations par Client
      public static  List<Reservation> getAllReservationByClient(Client client) {
        List<Reservation> reservationList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Reservation where CLEINT_IDENTIFIANT =?");
         query.setParameter(0,client.getCodeClient());
         reservationList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservationList;
    }
     
      
   // Recupérer la liste des Reservations par Client
      public static  List<Reservation> getAllReservationByArtcle(Article article) {
        List<Reservation> reservationList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Reservation where ARTICLE_REF =?");
         query.setParameter(0,article.getReferenceArticle());
         reservationList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservationList;
    }
      
      
     //Tester
     public static void main(String[] args) {
               // registerReservation(new  Reservation("1242", "Samsung S4", Float.NaN, 18, "Portable de Qualité", "Samsung", "",  "06 Rue de Madrid 90000", 90,"Belfort", null, null));
         /*for(Reservation u:ReservationDao.getAllReservationByClient(ClientDao.readerClient(Long.valueOf("1")))){
            System.out.println(" Reservation :" + u.getCodeReservation());
        }*/
    	 for(Reservation u:ReservationDao.getAllReservationByArtcle(ArticleDao.readerArticle(Long.valueOf("1")))){
             System.out.println(" Reservation :" + u.getCodeReservation());
         }
    	 
    }
    
}
