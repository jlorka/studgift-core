package  com.utbm.to.admin.core.catalogue.core.entity;

/***********************************************************************
 * Module:  Promotion.java
 * Author:  Marina
 * Purpose: Defines the Class Promotion
 ***********************************************************************/

import java.util.*;

public class Promotion {
   private long codePromotion;
   private String libellePromotion;
  
   //public Set<Article> article;

    public Promotion() {
    }

    public Promotion(long codePromotion, String valeurPromotion) {
        this.codePromotion = codePromotion;
        this.libellePromotion = valeurPromotion;
        //this.article = article;
    }

    /*public Set<Article> getArticle() {
        return article;
    }

    public void setArticle(Set<Article> article) {
        this.article = article;
    }*/

    public String getLibellePromotion() {
        return libellePromotion;
    }

    public void setLibellePromotion(String libellePromotion) {
        this.libellePromotion = libellePromotion;
    }

    public long getCodePromotion() {
        return codePromotion;
    }

    public void setCodePromotion(long codePromotion) {
        this.codePromotion = codePromotion;
    }

    
   
}