package com.utbm.to.admin.core.catalogue.core.dao;


import com.utbm.to.admin.core.catalogue.core.entity.Client;
import com.utbm.to.admin.core.catalogue.core.entity.Client;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 
 */
public class ClientDao {
    
    // Enregistrer un client
    public static void registerClient(Client client) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(client);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Modifier un client
     public static void updateClient(Client client) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.merge(client);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Lire un Categories
    public static Client readerClient(Long idClient) {
        Client client = new Client();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            client = (Client) session.get(Client.class, idClient);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return client;
    }

    // Recupérer la liste des Categories
     public static  List<Client> getAllClient() {
        List<Client> clientList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Client");
         clientList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return clientList;
    }
     
     
    
     
     
     public static void main(String[] args) {
         registerClient(new  Client(null, "Moulin", "Jacques", "06 Rue Madrid", "90000", "Belfort", "0689521412", "djibril3fr@yahoo.fr", "", "123456", new Date()));
        /*for(Client u:ClientDao.getAllClient()){
          System.out.println(" Client :" + u.getMail()+" "+ u.getMotDePasse());
       }*/
    }
    
}
