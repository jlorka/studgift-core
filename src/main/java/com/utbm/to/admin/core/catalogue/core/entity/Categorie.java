package  com.utbm.to.admin.core.catalogue.core.entity;

/***********************************************************************
 * Module:  Categorie.java
 * Author:  Marina
 * Purpose: Defines the Class Categorie
 ***********************************************************************/


public class Categorie {
   private Long codeCategorie;
   private String libelleCategorie;

    public Categorie(Long codeCategorie, String libelleCategorie) {
        this.codeCategorie = codeCategorie;
        this.libelleCategorie = libelleCategorie;
    }

    public Categorie() {
    }

    public void setCodeCategorie(Long codeCategorie) {
        this.codeCategorie = codeCategorie;
    }

    public Long getCodeCategorie() {
        return codeCategorie;
    }

    public String getLibelleCategorie() {
        return libelleCategorie;
    }

    public void setLibelleCategorie(String libelleCategorie) {
        this.libelleCategorie = libelleCategorie;
    }
   
   

}