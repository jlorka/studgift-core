package  com.utbm.to.admin.core.catalogue.core.entity;


/***********************************************************************
 * Module:  Catalogue.java
 * Author:  Marina
 * Purpose: Defines the Class Catalogue
 ***********************************************************************/

import java.util.*;

public class Catalogue {
   private Long codeCatalogue;
   private Date dateCreation;
   private String libelleCatalogue;

    public Catalogue(Long codeCatalogue, Date dateCreation, String libelleCatalogue) {
        this.codeCatalogue = codeCatalogue;
        this.dateCreation = dateCreation;
        this.libelleCatalogue = libelleCatalogue;
    }

    public Catalogue() {
    }

    public Long getCodeCatalogue() {
        return codeCatalogue;
    }

    public void setCodeCatalogue(Long codeCatalogue) {
        this.codeCatalogue = codeCatalogue;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getLibelleCatalogue() {
        return libelleCatalogue;
    }

    public void setLibelleCatalogue(String libelleCatalogue) {
        this.libelleCatalogue = libelleCatalogue;
    }
   
   

}