package com.utbm.to.admin.core.catalogue.core.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.utbm.to.admin.core.catalogue.core.entity.Article;
import com.utbm.to.admin.core.catalogue.core.entity.Client;
import com.utbm.to.admin.core.catalogue.core.entity.Acceptation;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 
 */
public class AcceptationDao {
    
    // Lire une reservation
    public static void registerAcceptation(Acceptation reservation) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(reservation);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Lire une reservation
    public static Acceptation readerAcceptation(Long idAcceptation) {
        Acceptation reservation = new Acceptation();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            reservation = (Acceptation) session.get(Acceptation.class, idAcceptation);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservation;
    }

    // Recupérer la liste des Acceptations
     public static  List<Acceptation> getAllAcceptation() {
        List<Acceptation> reservationList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Acceptation");
         reservationList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservationList;
    }
    
     // Recupérer la liste des Acceptations par Client
      public static  List<Acceptation> getAllAcceptationByClient(Client client) {
        List<Acceptation> reservationList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Acceptation where CLEINT_IDENTIFIANT =?");
         query.setParameter(0,client.getCodeClient());
         reservationList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservationList;
    }
     
      
   // Recupérer la liste des Acceptations par Client
      public static  List<Acceptation> getAllAcceptationByArtcle(Article article) {
        List<Acceptation> reservationList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Acceptation where ARTICLE_REF =?");
         query.setParameter(0,article.getReferenceArticle());
         reservationList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return reservationList;
    }
      
      
     //Tester
     public static void main(String[] args) {
               // registerAcceptation(new  Acceptation("1242", "Samsung S4", Float.NaN, 18, "Portable de Qualité", "Samsung", "",  "06 Rue de Madrid 90000", 90,"Belfort", null, null));
         /*for(Acceptation u:AcceptationDao.getAllAcceptationByClient(ClientDao.readerClient(Long.valueOf("1")))){
            System.out.println(" Acceptation :" + u.getCodeAcceptation());
        }*/
    	 for(Acceptation u:AcceptationDao.getAllAcceptationByArtcle(ArticleDao.readerArticle(Long.valueOf("1")))){
             System.out.println(" Acceptation :" + u.getCodeAcceptation());
         }
    	 
    }
    
}
