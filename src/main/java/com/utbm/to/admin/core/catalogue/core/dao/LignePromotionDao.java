package com.utbm.to.admin.core.catalogue.core.dao;


import com.utbm.to.admin.core.catalogue.core.entity.LignePromotion;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class LignePromotionDao {
    // Enregistrer un Ligne promotion    
    public static void registerLignePromotion(LignePromotion lignePromotion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(lignePromotion);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Lire une ligne Promotion
    public static LignePromotion readerLignePromotion(Date idLignePromotion) {
        LignePromotion lignePromotion = new LignePromotion();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            lignePromotion = (LignePromotion) session.get(LignePromotion.class, idLignePromotion);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return lignePromotion;
    }

    // Recupérer la liste des Promotions
     public static  List<LignePromotion> getAllLignePromotion() {
        List<LignePromotion> lignePromotionList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery("from LignePromotion");
         lignePromotionList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return lignePromotionList;
    }
     // Modifier une lignePromotion
      public static void updateLignePromotion(LignePromotion lignePromotion) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            readerLignePromotion(lignePromotion.getDebutPromotion());
            //lignePromotion.set(null);
            session.beginTransaction();
            session.merge(lignePromotion);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
     
     public static void main(String[] args) {
             //registerLignePromotion(new  LignePromotion(Long.parseLong("1"), Float.NaN, new Date(), new Date()));
         for(LignePromotion u:LignePromotionDao.getAllLignePromotion()){
            System.out.println(" LignePromotion :" + u.getArticle().getDesignation());
        }
    }
    
}
