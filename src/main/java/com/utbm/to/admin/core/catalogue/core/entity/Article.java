package  com.utbm.to.admin.core.catalogue.core.entity;

import java.sql.Blob;

/***********************************************************************
 * Module:  Article.java
 * Author:  Jibril
 * Purpose: Defines the Class Article
 ***********************************************************************/


public class Article {
    
	private  Long referenceArticle;
   private String designation;
   private Float prix;
   private long quantite;
   private String details;
   private String marque;
   private Blob image;
   private String adresse;
   private String codePostal;
   private String ville;
   private String activate;
   public Catalogue catalogue;
   public Categorie categorie;
   public Client client;
   

    public Article(Long referenceArticle, String designation, Float prix, long quantite, String details, String marque, String adresse, String codePostal, String ville, Catalogue catalogue, Categorie categorie) {
        this.referenceArticle = referenceArticle;
        this.designation = designation;
        this.prix = prix;
        this.quantite = quantite;
        this.details = details;
        this.marque = marque;
        
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.catalogue = catalogue;
        this.categorie = categorie;
    }
    public Article(Long referenceArticle, String designation, Float prix, long quantite, String details, String marque, Blob image, String adresse, String codePostal, String ville, Catalogue catalogue, Categorie categorie) {
        this.referenceArticle = referenceArticle;
        this.designation = designation;
        this.prix = prix;
        this.quantite = quantite;
        this.details = details;
        this.marque = marque;
        this.image = image;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.catalogue = catalogue;
        this.categorie = categorie;
    }
   

    public Article() {
    }
    
    
    public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setCatalogue(Catalogue catalogue) {
        this.catalogue = catalogue;
    }

    public Catalogue getCatalogue() {
        return catalogue;
    }

   

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

   
    public Blob getImage() {
		return image;
	}
	public void setImage(Blob image) {
		this.image = image;
	}
	public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public long getQuantite() {
        return quantite;
    }

    public void setQuantite(long quantite) {
        this.quantite = quantite;
    }

   

    public Long getReferenceArticle() {
		return referenceArticle;
	}



	public void setReferenceArticle(Long referenceArticle) {
		this.referenceArticle = referenceArticle;
	}



	public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
	public String getActivate() {
		return activate;
	}
	public void setActivate(String activate) {
		this.activate = activate;
	}
     
   
}