package com.utbm.to.admin.core.catalogue.core.dao;


import com.utbm.to.admin.core.catalogue.core.entity.Categorie;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 
 */
public class CategorieDao {
    
        // Enregister une Categorie
    public static void registerCategorie(Categorie categorie) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(categorie);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
        // Lire une Categorie
    public static Categorie readerCategorie(Long idCategorie) {
        Categorie categorie = new Categorie();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            categorie = (Categorie) session.get(Categorie.class, idCategorie);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return categorie;
    }

            // Recupérer la liste des Categories
     public static  List<Categorie> getAllCategorie() {
        List<Categorie> categorieList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Categorie");
         categorieList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return categorieList;
    }
     
     
     public static void main(String[] args) {
         registerCategorie(new  Categorie(null, "Bandes Dessinées"));
         
          // for(Categorie u:CategorieDao.getAllCategorie()){
         //;
           // System.out.println(" Categorie :" + readerCategorie(Long.parseLong("1")).getLibelleCategorie());
        //}
    }
    
}
