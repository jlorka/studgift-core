package  com.utbm.to.admin.core.catalogue.core.entity;

/***********************************************************************
 * Module:  Client.java
 * Author:  Jibril
 * Purpose: Defines the Class Client
 ***********************************************************************/

import java.util.*;

public class Client {
   private  Long  codeClient;
   private String nomClient;
   private String prenomClient;
   private String rue;
   private String codePostal;
   private String ville;
   private String telephone;
   private String mail;
   private String identifiant;
   private String motDePasse;
   private Date dateNais;
   
   public List<Reservation> reservations;

    
   
   /** @pdOid 929af031-755f-4a3b-8a12-63b317e73274 */
   public void demandeAfficgaeCatalogue() {
      // TODO: implement
   }

    public Client() {
    }

    public Client(Long codeClient, String nomClient, String prenomClient, String rue, String codePostal, String ville, String telephone, String mail, String identifiant, String motDePasse, Date dateNais, List<Reservation> reservations) {
        this.codeClient = codeClient;
        this.nomClient = nomClient;
        this.prenomClient = prenomClient;
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.mail = mail;
        this.identifiant = identifiant;
        this.motDePasse = motDePasse;
        this.dateNais = dateNais;
        this.reservations = reservations;
    }
    
    public Client(Long codeClient, String nomClient, String prenomClient, String rue, String codePostal, String ville, String telephone, String mail, String identifiant, String motDePasse, Date dateNais) {
        this.codeClient = codeClient;
        this.nomClient = nomClient;
        this.prenomClient = prenomClient;
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.mail = mail;
        this.identifiant = identifiant;
        this.motDePasse = motDePasse;
        this.dateNais = dateNais;
    }

    public Date getDateNais() {
        return dateNais;
    }

    public void setDateNais(Date dateNais) {
        this.dateNais = dateNais;
    }
    
   
    public Long getCodeClient() {
        return codeClient;
    }

    public void setCodeClient(Long codeClient) {
        this.codeClient = codeClient;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public String getPrenomClient() {
        return prenomClient;
    }

    public void setPrenomClient(String prenomClient) {
        this.prenomClient = prenomClient;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
   
   
   
   /** @pdOid 3712705d-c8f9-4668-897a-5fe1684573d5 */
   public void creerReservation() {
      // TODO: implement
   }
   
   
  
}