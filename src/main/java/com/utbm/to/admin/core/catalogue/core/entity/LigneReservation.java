package  com.utbm.to.admin.core.catalogue.core.entity;

/***********************************************************************
 * Module:  LigneReservation.java
 * Author:  Marina
 * Purpose: Defines the Class LigneReservation
 ***********************************************************************/

import java.util.*;

public class LigneReservation {
   private int quantiteReservee;
   private Reservation reservation;
   private Article article;

    public LigneReservation() {
    }

    public LigneReservation(int quantiteReservee, Reservation reservation, Article article) {
        this.quantiteReservee = quantiteReservee;
        this.reservation = reservation;
        this.article = article;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public int getQuantiteReservee() {
        return quantiteReservee;
    }

    public void setQuantiteReservee(int quantiteReservee) {
        this.quantiteReservee = quantiteReservee;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    
     

}