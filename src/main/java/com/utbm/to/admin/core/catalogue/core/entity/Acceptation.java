package  com.utbm.to.admin.core.catalogue.core.entity;


/***********************************************************************
 * Module:  Acceptation.java
 * Author:  
 * Purpose: Defines the Class Acceptation
 ***********************************************************************/

import java.util.*;

public class Acceptation {
   private long codeAcceptation;
   private Date dateAcceptation;
   private Client client;
   private Article article;
   
    public Acceptation() {
    }

    public Acceptation(long codeAcceptation, Date dateDeAcceptation, Client client) {
        this.codeAcceptation = codeAcceptation;
        this.dateAcceptation = dateDeAcceptation;
        this.client = client;
    }

    public Client getClient() {
        return client;
    }

	public long getCodeAcceptation() {
		return codeAcceptation;
	}

	public void setCodeAcceptation(long codeAcceptation) {
		this.codeAcceptation = codeAcceptation;
	}

	public Date getDateAcceptation() {
		return dateAcceptation;
	}

	public void setDateAcceptation(Date dateAcceptation) {
		this.dateAcceptation = dateAcceptation;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public void setClient(Client client) {
		this.client = client;
	}

   
    
 
}