package com.utbm.to.admin.core.catalogue.core.dao;


import com.utbm.to.admin.core.catalogue.core.entity.Catalogue;
import com.utbm.to.admin.core.catalogue.core.tool.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * 
 */
public class CatalogueDao {
    
    // Enregister un Catalogue
    public static void registerCatalogue(Catalogue catalogue) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(catalogue);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    // Lire un Catalogue
    public static Catalogue readerCatalogue(Long idCatalogue) {
        Catalogue catalogue = new Catalogue();
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            catalogue = (Catalogue) session.get(Catalogue.class, idCatalogue);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return catalogue;
    }

        // Afficher la liste des Catalogues
     public static  List<Catalogue> getAllCatalogue() {
        List<Catalogue> catalogueList=null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
   
         Query query = session.createQuery(" from Catalogue");
         catalogueList = query.list();
        } catch (HibernateException he) {
            he.printStackTrace();
            if (session.getTransaction() != null) {
                try {
                    session.getTransaction().rollback();
                } catch (HibernateException he2) {
                    he2.printStackTrace();
                }
            }
        }
        return catalogueList;
    }
     
     
     public static void main(String[] args) {
         registerCatalogue(new  Catalogue(null, new Date() , "Tenue d'Eté"));
           //for(Catalogue u:CatalogueDao.getAllCatalogue()){
            //System.out.println(" Catalogue :" + readerCatalogue(Long.parseLong("2")).getLibelleCatalogue());
        //}
    }
    
}
